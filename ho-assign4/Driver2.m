//
//  Driver2.m
//  ho-assign4
//
//  Created by Henrique de Oliveira Carvalho on 2014-09-30.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 2:
//  Design and implement a class called PairOfDice, composed of two Die objects.
//  Include methods to set and get the individual die values, a method to roll
//  both dice, and a method that returns the current sum of the two die values.
//  Also, include a description method that returns a one line description of
//  both dice.
//
// Inputs:   none
// Outputs:  description of the dice
//
// ******************************************************************************

#import "Driver2.h"
#import "PairOfDice.h"

@implementation Driver2
- (void) run
{
  // Object dice
  PairOfDice * dice = [[PairOfDice alloc] init];

  // Initialization of dice
  [dice initPairOfDice];

  // Roll both dice
  [dice rollBothDice];

  // Testing sum of the two die values
  NSLog(@"Sum of dice: %d", [dice sumOfDice]);

  NSLog(@"%@", dice);
}
@end
