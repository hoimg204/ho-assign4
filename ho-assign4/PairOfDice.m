//
//  PairOfDice.m
//  ho-assign4
//
//  Created by Henrique de Oliveira Carvalho on 2014-09-30.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 2:
//  Design and implement a class called PairOfDice, composed of two Die objects.
//  Include methods to set and get the individual die values, a method to roll
//  both dice, and a method that returns the current sum of the two die values.
//  Also, include a description method that returns a one line description of
//  both dice.
//
// Inputs:   none
// Outputs:  description of the dice
//
// ******************************************************************************


#import "PairOfDice.h"
#import "Die.h"

@implementation PairOfDice
{
  Die * dieOne;
  Die * dieTwo;
}

// function to initialize the objects
- (void) initPairOfDice
{
  dieOne = [[Die alloc] init];
  dieTwo = [[Die alloc] init];
}

- (void) rollBothDice
{
  [dieOne roll];
  [dieTwo roll];
}

- (int) sumOfDice
{
  return dieOne.getFaceValue + dieTwo.getFaceValue;
}

// method "description" to display values of instance variables
- (NSString *) description
{
  NSString * str = @"";

  str = [str stringByAppendingFormat:@"Die One: %d ", [dieOne getFaceValue] ];
  str = [str stringByAppendingFormat:@"Die Two: %d ", [dieTwo getFaceValue] ];
  str = [str stringByAppendingFormat:@"Sum: %d", self.sumOfDice];
  return str;
}

@end
