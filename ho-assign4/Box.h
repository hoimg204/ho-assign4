//
//  Box.h
//  ho-assign4
//
//  Created by Henrique de Oliveira Carvalho on 2014-09-30.
//  Copyright (c) 2014 beta. All rights reserved.
//
/// Problem Statement 1:
//  Design and implement a class called Box that contains instance data that
//  represents the height, width, and depth of the box. Also include a boolean
//  variable called full as instance data that represents whether the box is
//  full or not. Define the Box constructor to accept and initialize the
//  height, width, and depth of the box. Each newly created Box is empty (the
//  constructor should initialize full to false). Include getter and setter
//  methods for all instance data. Include a description method that returns a
//  one line description of the box.
//
//
// Inputs:   none
// Outputs:  description of the box
//
// ******************************************************************************

#import <Foundation/Foundation.h>

@interface Box : NSObject

// custom constructor
- (id) initWithHeight:(double)boxHeight andWidth:(double)boxWidth andDepth:(double)boxDepth;

// setters and getters
- (void) setHeight:(double)boxHeight;
- (double) getHeight;

- (void) setWidth:(double)boxWidth;
- (double) getWidth;

- (void) setDepth:(double)boxDepth;
- (double) getDepth;

- (void) setFull:(double)boxFull;
- (Boolean) getFull;

// method "description" to display values of instance variables
- (NSString *) description;

@end
