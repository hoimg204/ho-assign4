//
//  Driver1.m
//  ho-assign4
//
//  Created by Henrique de Oliveira Carvalho on 2014-09-30.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 1:
//  Design and implement a class called Box that contains instance data that
//  represents the height, width, and depth of the box. Also include a boolean
//  variable called full as instance data that represents whether the box is
//  full or not. Define the Box constructor to accept and initialize the
//  height, width, and depth of the box. Each newly created Box is empty (the
//  constructor should initialize full to false). Include getter and setter
//  methods for all instance data. Include a description method that returns a
//  one line description of the box.
//
//
// Inputs:   none
// Outputs:  description of the box
//
// ******************************************************************************

#import "Driver1.h"
#import "Box.h"

@implementation Driver1
- (void) run
{
  Box * box1 = [[Box alloc] init];

  // Testing setters
  [box1 setHeight:12.0];
  [box1 setWidth:12.0];
  [box1 setDepth:12.0];

  // Testing getters
  NSLog(@"box1 H: %f W: %f D: %f Full: %hhu", [box1 getHeight], [box1 getWidth], [box1 getDepth], [box1 getFull]);

  Box * box2 = [[Box alloc] init];
  // Testing setters
  [box2 setHeight:14.0];
  [box2 setWidth:14.0];
  [box2 setDepth:14.0];

  // Testing getters
  NSLog(@"box2 H: %f W: %f D: %f Full: %hhu", [box2 getHeight], [box2 getWidth], [box2 getDepth], [box2 getFull]);

  // Descriptions
  NSLog(@"box1 %@", box1);
  NSLog(@"box2 %@", box2);
}
@end
