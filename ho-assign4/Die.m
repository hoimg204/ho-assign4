//
//  Die.m
//  ho-assign4
//
//  Created by Henrique de Oliveira Carvalho on 2014-09-30.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 2:
//  Design and implement a class called PairOfDice, composed of two Die objects.
//  Include methods to set and get the individual die values, a method to roll
//  both dice, and a method that returns the current sum of the two die values.
//  Also, include a description method that returns a one line description of
//  both dice.
//
// Inputs:   none
// Outputs:  description of the dice
//
// ******************************************************************************


#include <stdlib.h>
#import "Die.h"

@implementation Die
{
  int iFaceValue;
}

- (void) setFaceValue:(int)dieFaceValue
{
  iFaceValue = dieFaceValue;
}

- (int) getFaceValue
{
  return iFaceValue;
}

- (void) roll
{
  iFaceValue = (arc4random() % 6) + 1;
}


@end

