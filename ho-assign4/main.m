//
//  main.m
//  ho-assign4
//
//  Created by Henrique de Oliveira Carvalho on 2014-09-30.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 1:
//  Design and implement a class called Box that contains instance data that
//  represents the height, width, and depth of the box. Also include a boolean
//  variable called full as instance data that represents whether the box is
//  full or not. Define the Box constructor to accept and initialize the
//  height, width, and depth of the box. Each newly created Box is empty (the
//  constructor should initialize full to false). Include getter and setter
//  methods for all instance data. Include a description method that returns a
//  one line description of the box.
//
//
// Inputs:   none
// Outputs:  description of the box
//
// ******************************************************************************
//
// Problem Statement 2:
//  Design and implement a class called PairOfDice, composed of two Die objects.
//  Include methods to set and get the individual die values, a method to roll
//  both dice, and a method that returns the current sum of the two die values.
//  Also, include a description method that returns a one line description of
//  both dice.
//
// Inputs:   none
// Outputs:  description of the dice
//
// ******************************************************************************

#import <Foundation/Foundation.h>
#import "Driver1.h"
#import "Driver2.h"

int main(int argc, const char * argv[])
{
  @autoreleasepool {
    // Object box
    Driver1 * box = [[Driver1 alloc] init];
    [box run];

    // Object dice
    Driver2 * dice = [[Driver2 alloc] init];
    [dice run];
  }
  return 0;
}
