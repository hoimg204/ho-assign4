//
//  Box.m
//  ho-assign4
//
//  Created by Henrique de Oliveira Carvalho on 2014-09-30.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 1:
//  Design and implement a class called Box that contains instance data that
//  represents the height, width, and depth of the box. Also include a boolean
//  variable called full as instance data that represents whether the box is
//  full or not. Define the Box constructor to accept and initialize the
//  height, width, and depth of the box. Each newly created Box is empty (the
//  constructor should initialize full to false). Include getter and setter
//  methods for all instance data. Include a description method that returns a
//  one line description of the box.
//
//
// Inputs:   none
// Outputs:  description of the box
//
// ******************************************************************************

#import "Box.h"

@implementation Box
{
  double dHeight;
  double dWidth;
  double dDepth;
  Boolean bFull;
}

- (id) initWithHeight:(double)boxHeight andWidth:(double)boxWidth andDepth:(double)boxDepth
{
  self = [super init];
  if (self)
  {
    // initialization happens here ...
    dHeight = boxHeight;
    dWidth  = boxWidth;
    dDepth  = boxDepth;
    bFull = false;
  }
  return self;
}

// Height: setter and getter
- (void) setHeight:(double)boxHeight
{
  dHeight = boxHeight;
}

- (double) getHeight
{
  return dHeight;
}

// Width: setter and getter
- (void) setWidth:(double)boxWidth
{
  dWidth = boxWidth;
}

- (double) getWidth
{
  return dWidth;
}

// Depth: setter and getter
- (void) setDepth:(double)boxDepht
{
  dDepth = boxDepht;
}

- (double) getDepth
{
  return dDepth;
}

// Full: setter and getter
- (void) setFull:(double)boxFull
{
  bFull = boxFull;
}

- (Boolean) getFull
{
  return bFull;
}

// method "description" to display values of instance variables
- (NSString *) description
{
  NSString * str = @"";

  str = [str stringByAppendingString:@"Height: "];
  str = [str stringByAppendingFormat:@"%f", dHeight];
  str = [str stringByAppendingString:@"\tWidth: "];
  str = [str stringByAppendingFormat:@"%f", dWidth];
  str = [str stringByAppendingString:@"\tDepth: "];
  str = [str stringByAppendingFormat:@"%f", dDepth];
  str = [str stringByAppendingString:@"\tFull: "];
  str = [str stringByAppendingFormat:@"%s", ( bFull ? "full" : "empty") ];
  return str;
}


@end
