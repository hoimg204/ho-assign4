//
//  Die.h
//  ho-assign4
//
//  Created by Henrique de Oliveira Carvalho on 2014-09-30.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 2:
//  Design and implement a class called PairOfDice, composed of two Die objects.
//  Include methods to set and get the individual die values, a method to roll
//  both dice, and a method that returns the current sum of the two die values.
//  Also, include a description method that returns a one line description of
//  both dice.
//
// Inputs:   none
// Outputs:  description of the dice
//
// ******************************************************************************


#import <Foundation/Foundation.h>

@interface Die : NSObject

- (void) setFaceValue:(int)dieFaceValue;
- (int) getFaceValue;
- (void) roll;

@end
